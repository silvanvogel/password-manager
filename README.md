# KURAGARI - Key Guardian

## Description

This project is a password manager written in C for the I2C course. It aims to securely store and manage your passwords.

## Latest Release

KURAGARI Version 1.0.0 is now available! This release includes binaries for both Linux and Windows.

[Download the latest release here](https://gitlab.com/silvanvogel/password-manager/-/releases/1.0.0)

## Getting started

### Prerequisites

* GCC compiler
* Make
* libssl-dev

#### Prerequisites for Windows Cross-Compilation

* mingw-w64

### Clone the Repository

To clone the repository, use:
```bash
git clone https://USERNAME:ACCESSTOKEN@gitlab.com/silvanvogel/password-manager.git 
```

### Initial Setup

After cloning the repository, run the following commands to prevent tracking changes in the `data/` directory:

```bash
git update-index --assume-unchanged data/master.hash
git update-index --assume-unchanged data/accounts.db
```

This will stop git from tracking changes in the master.hash and the accounts.db files.
If changes need to be tracked again, this can be reverted with:

```bash
git update-index --no-assume-unchanged data/master.hash
git update-index --no-assume-unchanged data/accounts.db
```

## Working with Branches

### Creating a New Branch

To create and switch to a new branch:

```bash
git checkout -b <branch-name>
```

Replace <branch-name> with a meaningful name for the new branch.

### Switching Between Branches

To switch to an existing branch:

```bash
git checkout <branch-name>
```

### Merging Changes

Before merging, make sure you are on the branch you want to merge into:

```bash
git checkout <target-branch>
```

Then merge another branch into the current branch:

```bash
git merge <source-branch>
```

Resolve any merge conflicts if they arise.

### Pushing Branches to Remote

To push a new branch to the remote repository:

```bash
git push -u origin <branch-name>
```

### Pulling Changes from Remote

To update the local branch with changes from the remote:

```bash
git pull
```

## Building the Project

### For Debian-based systems

To build the project, navigate to the root directory of the project and run:
```bash
make
```

This will compile the source files and create an executable in the `bin/linux/` directory.

### For Windows (Cross-Compiling)

To cross-compile the project for Windows, use the `Makefile.win` with the following command:

```bash
make -f Makefile.win
```

Ensure MinGW-w64 is installed and the path to the Windows OpenSSL libraries in `Makefile.win` is correctly set. 
This will create a Windows executable in the `bin/windows/` directory.

## Cleaning the Project

If you need to clean up the project (remove all compiled files), run:

```bash
make clean
```

This will remove all the object files and the compiled executable.

## Folder Structure

Here's an overview of the main folders in the project and their intended use:

* src/: Contains source code files (.c files).
* include/: Contains header files (.h files).
* data/: Contains account database (.db file) and master password (.hash file).
* lib/: Contains external libraries.
* build/:
    * bin/: Contains compiled executable(s).
    * obj/: Contains intermediate object files (.o files).
* test/: Contains unit tests.
