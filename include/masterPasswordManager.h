#ifndef MASTER_PASSWORD_MANAGER_H
#define MASTER_PASSWORD_MANAGER_H

// Constant
#define MASTER_PASSWORD_MAX_LEN 100

// Path to file where master password hash is stored.
// Contains hashed version of master password.
extern const char* MASTER_HASH_FILE;

/**
 * Sets the master password by storing its hash.
 * 
 * @param masterPassword The master password to be set.
 * @return 0 if successful, non-zero otherwise.
 */
int setMasterPassword(const char* masterPassword);

/**
 * Verifies if the provided master password matches the stored hash.
 * 
 * @param masterPassword The master password to be verified.
 * @return 0 if the password matches, non-zero otherwise.
 */
int verifyMasterPassword(const char* masterPassword);

/**
 * Changes the master password. Verifies the old master password before setting the new one.
 * 
 * @param oldMasterPassword The current master password.
 * @param newMasterPassword The new master password to be set.
 * @return 0 if the password was successfully changed, non-zero otherwise.
 */
int changeMasterPassword(const char* oldMasterPassword, const char* newMasterPassword);

#endif // MASTER_PASSWORD_MANAGER_H

