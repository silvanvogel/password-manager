#ifndef PASSWORD_MANAGER_H
#define PASSWORD_MANAGER_H

#include <stddef.h>

// Path to file where individual accounts are stored.
// Acts as database for user accounts.
extern const char* ACCOUNTS_FILE;

// Define the Account struct
typedef struct {
    char name[100];  // Adjust size as needed
    char username[100]; // Adjust size as needed
    char password[100]; // Adjust size as needed
} Account;

/**
 * Retrieves all accounts from the password database.
 *
 * @param count A pointer to store the number of accounts retrieved.
 * @return A dynamically allocated array of Account structures or NULL if none found.
 *         The caller is responsible for freeing this memory.
 */
Account* getAllAccounts(size_t* count);

/**
 * Adds a new password entry for a specific account.
 * 
 * @param account The account for which the password is being stored.
 * @return 0 if successful, non-zero otherwise.
 */
int addAccount(const Account* account);

/**
 * Retrieves the password of a specific account by index.
 * 
 * @param account The account for which the password should be fetched.
 * @return Dynamically allocated string containing the password, or NULL if not found.
 *         The caller is responsible for freeing this memory.
 */
char* getPassword(const Account* account);

/**
 * Updates an account in the password database.
 * 
 * @param index The index of the account to be updated.
 * @param newAccount The updated account information.
 * @return 0 if successful, non-zero otherwise.
 */
int updateAccount(size_t index, const Account* newAccount);

/**
 * Deletes an account from the password database.
 * 
 * This function removes the account at the specified index from the database.
 * It adjusts the remaining accounts in the database to fill in the gap
 * created by the deleted account and updates the database file accordingly.
 *
 * @param index The index of the account to be deleted.
 * @return 0 if successful, non-zero otherwise.
 */
int deleteAccount(size_t index);

/**
 * Checks the strength of the provided master password.
 * 
 * @param password The master password to be checked.
 * @return int The strength score of the password.
 */
int checkPasswordStrength(const char* password);

/**
 * Generates a secure password of a specified length.
 *
 * @param length Length of the password to be generated.
 * @return Pointer to a dynamically allocated string containing the password.
 *         The caller is responsible for freeing this memory.
*/
char* generateSecurePassword(int length);

#endif // PASSWORD_MANAGER_H


