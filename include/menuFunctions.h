#ifndef MENU_FUNCTIONS_H
#define MENU_FUNCTIONS_H

#include <stddef.h>
#include "passwordManager.h"

// Constants
#define MENU_OPTIONS_COUNT 5                                                        // Number of options in the main menu
#define ACCOUNTS_MENU_OPTIONS_COUNT 6                                               // Number of options in the account menu
#define MASTER_PASSWORD_MENU_OPTIONS_COUNT 2                                        // Number of options in the master password menu

#define NAME_COL_WIDTH 25                                                           // Name column width for account table
#define USERNAME_COL_WIDTH 25                                                       // Username column width for account table 
#define NO_COL_WIDTH 4                                                              // Number column width for account table

// Global variable declarations
extern const char* mainMenuOptions[MENU_OPTIONS_COUNT];                             // Options for the main menu
extern const char* accountsMenuOptions[ACCOUNTS_MENU_OPTIONS_COUNT];                // Options for the accounts menu
extern const char* masterPasswordMenuOptions[MASTER_PASSWORD_MENU_OPTIONS_COUNT];   // Options for the master password menu

/**
 * Clears the console screen.
 *
 * This function clears the console screen to provide a clean interface for the user.
 * Prints new lines to simulate the clearing - to avoid system calls.
 */
void clearScreen();

/**
 * Displays name of the password manager.
 *
 * This function prints the name of the password manager to the console.
 */
void displayHeader();

/**
 * Displays a standardized title for menus
 *
 * @param title The title string to be displayed
 */
void displayMenuTitle(const char* title);

/**
 * @brief Pauses execution and waits for the user to press Enter. After the user
 * presses Enter, the specified next function is executed.
 *
 * @param nextFunction The function to be executed after the pause. This should be
 * a void function with no parameters.
 */
void pauseAndExecute(void (*nextFunction)(void));

/**
 * Clears the input buffer to handle unexpected inputs.
 */
void clearBuffer();

/**
 * Prints the header for the accounts table.
 */
void printAccountTableHeader();

/**
 * Prints the footer for the accounts table.
 */
void printAccountTableFooter();

/**
 * Prints a single account in a table row format
 */
void printAccountRow(const Account* account, size_t index);

/**
 * Verifies master password at the start of the program.
 *
 * If master password file is empty (indicating first-time use), 
 * prompts user to set new master password. Otherwise, it prompts 
 * user to enter master password to unlock the password manager.
 *
 * @return Returns zero if master password is verified successfully, non-zero otherwise.
 */
int verifyMasterPasswordOnStartup();

/**
 * @brief Prompts the user for the current master password, then
 * for a new master password, and finally changes the master password
 * if the current one is correct and the new passwords match.
 */
void changeMasterPasswordProcedure();

/**
 * @brief Displays a menu for checking the strength of a password.
 * Allows the user to input a password and displays its strength score.
 */
void checkPasswordStrengthMenu();

/**
 * Displays a menu for generating a secure password.
 */
void generatePasswordMenu();

/**
 * Displays a list of all accounts stored in the password database.
 *
 * This function fetches all accounts and displays their names and usernames. 
 * It handles memory management for the accounts list.
 */
void listAllAccountsMenu();

/**
 * Displays the menu for adding a new account.
 *
 * This function provides the interface to add a new account, including service name,
 * username, and password. It calls the appropriate function to store the new account
 * information securely.
 */
void addAccountMenu();

/**
 * Displays a menu to retrieve and display the password of a selected account.
 */
void getPasswordMenu();

/**
 * Displays a menu for updating an account.
 * Allows the user to select an account and then enter new account details.
 */
void updateAccountMenu();

/**
 * Displays a menu to delete an account.
 * Allows the user to select an account to delete from the displayed list of accounts.
 * On user confirmation, it deletes the selected account from the database.
 */
void deleteAccountMenu(void);

/**
 * Displays the menu for managing the master password.
 *
 * This function allows the user to change the master password or return to the main menu.
 */
void manageMasterPasswordMenu();

/**
 * Displays the menu for managing individual accounts.
 *
 * This function provides options to list services, add, retrieve, update, and delete accounts,
 * as well as to return to the main menu.
 */
void manageAccountsMenu();

/**
 * Displays the main menu of the password manager.
 *
 * This function provides the primary interface for the user to interact with the password manager,
 * offering options to manage passwords, manage the master password, or exit the application.
 */
void displayMainMenu();

#endif // MENU_FUNCTIONS_H

