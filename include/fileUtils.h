#ifndef FILE_UTILS_H
#define FILE_UTILS_H

#include <stdio.h>

/**
 * Custom implementation of getline function for cross-platform compatibility.
 *
 * This function reads an entire line from a stream, storing the address of
 * the buffer containing the text into *lineptr. The buffer is null-terminated
 * and includes the newline character, if one was found.
 *
 * If *lineptr is set to NULL and *n is set 0 before the call, then
 * custom_getline() will allocate a buffer for storing the line. This buffer
 * should be freed by the user program even if custom_getline() failed.
 *
 * @param lineptr Pointer to the buffer holding the line contents.
 * @param n Pointer to the size of the buffer.
 * @param stream The file stream to read from.
 * @return The number of characters read, including the delimiter character,
 *         but not including the terminating null byte. This value can be used
 *         to handle dynamic buffer resizing.
 */
ssize_t custom_getline(char **lineptr, size_t *n, FILE *stream);

/**
 * Checks if a file is empty.
 *
 * This function opens the specified file and checks if its size is zero,
 * indicating that the file is empty.
 *
 * @param filename The name of the file to check.
 * @return 0 if file is empty, non-zero otherwise.
 */
int isFileEmpty(const char *filename);

/**
 * Reads all lines from a given file.
 * 
 * @param filename The name of the file to be read.
 * @return A dynamically allocated array of strings, each representing a line.
 *         The array is terminated by a NULL pointer. Returns NULL if the file
 *         cannot be opened or if the file is empty.
 */
char** readAllLinesFromFile(const char *filename);

/**
 * Appends a line to a file.
 * 
 * @param filename The name of the file to append to.
 * @param line The line to append to the file.
 * @return 0 on success, non-zero on failure.
 */
int appendLineToFile(const char *filename, const char *line);

#endif // FILE_UTILS_H

