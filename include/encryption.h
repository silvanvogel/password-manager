#ifndef ENCRYPTION_H
#define ENCRYPTION_H

#include <stddef.h>

#define SALT_SIZE 16   // Size of the salt
#define HASH_SIZE 32   // Size of the hash (SHA-256)

// Key for AES-256 encryption. In a real-world application, this key should be securely generated and stored.
// Hardcoded keys, as shown here, are NOT recommended for production use due to security risks.
// The key length for AES-256 is 32 bytes (256 bits).
static const unsigned char key[] = {
    0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0,
    0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0,
    0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0,
    0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0
};

// Initialization Vector (IV) for AES encryption. 
// The IV should be unique and unpredictable for each encryption operation to ensure security.
// Hardcoded or predictable IVs, as used here, are not secure for production environments.
// The IV length for AES is 16 bytes (128 bits).
static const unsigned char iv[] = {
    0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
    0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF
};

/**
 * Hashes the provided data using SHA-256.
 * 
 * Generates a random salt and uses it along with the provided data
 * to produce a cryptographic hash. The salt and the hash are returned through
 * the output parameters.
 *
 * For more information see: https://wiki.openssl.org/index.php/EVP_Message_Digests
 * 
 * @param data Data to be hashed.
 * @param dataLen Length of data to be hashed.
 * @param salt Buffer to store generated salt. Buffer size should be at least SALT_SIZE.
 * @param hash Buffer to store generated hash. Buffer size should be at least HASH_SIZE.
 * @param generateSalt Flag to indicate whether to generate new salt (non-zero) or use provided salt (zero).
 * @return 0 on success, non-zero on failure.
 */
int hashData(const unsigned char* data, size_t dataLen, unsigned char* salt, unsigned char* hash, int generateSalt);

/**
 * Encrypts a plaintext password using AES-256 in CBC mode.
 *
 * This function takes a plaintext password and encrypts it using AES-256 encryption
 * in CBC (Cipher Block Chaining) mode. The function uses a predefined encryption key
 * and initialization vector (IV). The output is a hexadecimal string representing
 * the encrypted data, suitable for storage.
 *
 * Note: In a production environment, ensure secure key management and avoid hardcoding keys.
 *       For information regarding implementation see: https://github.com/openssl/openssl/blob/master/demos/encrypt/rsa_encrypt.c
 *
 * @param plaintext The plaintext password to be encrypted.
 * @return Dynamically allocated string containing the encrypted password in hex format,
 *         or NULL if encryption fails. The caller is responsible for freeing this memory.
 */
char* encryptPassword(const char* plaintext);

/**
 * Decrypts an encrypted password hex string using AES-256 in CBC mode.
 *
 * This function takes a hex string representing an encrypted password and decrypts
 * it using AES-256 decryption in CBC mode. The function uses the same encryption key
 * and initialization vector (IV) as used in the encryption process.
 *
 * Note: The hex string should be the direct output of the encryptPassword function.
 *       For information regarding implementation see: https://github.com/openssl/openssl/blob/master/demos/encrypt/rsa_encrypt.c
 *
 * @param hex_ciphertext The hex string representing the encrypted password.
 * @return Dynamically allocated string containing the decrypted plaintext password,
 *         or NULL if decryption fails. The caller is responsible for freeing this memory.
 */
char* decryptPassword(const char* hex_ciphertext);

#endif // ENCRYPTION_H

