#include "masterPasswordManager.h"
#include "encryption.h"
#include "fileUtils.h"
#include <openssl/evp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

// Path to master password file
const char* MASTER_HASH_FILE = "data/master.hash";

int setMasterPassword(const char* masterPassword) {
    unsigned char salt[SALT_SIZE];
    unsigned char hash[HASH_SIZE];

    if (strlen(masterPassword) >= MASTER_PASSWORD_MAX_LEN) {
        // Master password too long
        return 1;
    }

    // Hash the master password using hashData from encryption.c
    if (hashData((const unsigned char*)masterPassword, strlen(masterPassword), salt, hash, 1) != 0) {
        return 1; // Hashing failed
    }

    // Open the file to write the salt and hash
    FILE* file = fopen(MASTER_HASH_FILE, "wb");
    if (file == NULL) {
        // Error: File opening failed
        return 1;
    }

    // Write the salt
    if (fwrite(salt, 1, sizeof(salt), file) != sizeof(salt)) {
        // Error: File write failed
        fclose(file);
        return 1;
    }

    // Write the hash
    if (fwrite(hash, 1, sizeof(hash), file) != sizeof(hash)) {
        // Error: File write failed
        fclose(file);
        return 1;
    }

    // Close the file
    fclose(file);

    return 0; // Success
}

int verifyMasterPassword(const char* masterPassword) {
    unsigned char stored_salt[SALT_SIZE];
    unsigned char stored_hash[HASH_SIZE];
    unsigned char computed_hash[HASH_SIZE];

    if (strlen(masterPassword) >= MASTER_PASSWORD_MAX_LEN) {
        // Master password too long
        return 1;
    }

    // Open the file to read the salt and hash
    FILE* file = fopen(MASTER_HASH_FILE, "rb");
    if (file == NULL) {
        // Error: File opening failed
        return 1;
    }

    // Read the salt
    if (fread(stored_salt, 1, sizeof(stored_salt), file) != sizeof(stored_salt)) {
        // Error: File read failed
        fclose(file);
        return 1;
    }

    // Read the hash
    if (fread(stored_hash, 1, sizeof(stored_hash), file) != sizeof(stored_hash)) {
        // Error: File read failed
        fclose(file);
        return 1;
    }

    // Close the file
    fclose(file);

    // Hash the provided master password using hashData
    if (hashData((const unsigned char*)masterPassword, strlen(masterPassword), stored_salt, computed_hash, 0) != 0) {
        return 1; // Hashing failed
    }

    // Compare the computed hash with the stored hash
    if (memcmp(stored_hash, computed_hash, HASH_SIZE) == 0) {
        return 0; // Password matches
    } else {
        return 1; // Password does not match
    }
}

int changeMasterPassword(const char* oldMasterPassword, const char* newMasterPassword) {
    // Verify the old master password
    if (verifyMasterPassword(oldMasterPassword) == 0) {
        // Set new master password
        return setMasterPassword(newMasterPassword);
    } else {
        return 1;
    }
}
