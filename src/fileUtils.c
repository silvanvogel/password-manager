#include "fileUtils.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Custom function to read a line from a file
ssize_t custom_getline(char **lineptr, size_t *n, FILE *stream) {
    if (lineptr == NULL || n == NULL || stream == NULL) {
        return -1;
    }

    const size_t buffer_increment = 128;
    size_t current_buffer_size = *n;
    char *buffer = *lineptr;

    if (buffer == NULL || current_buffer_size == 0) {
        current_buffer_size = buffer_increment;
        buffer = (char *)malloc(current_buffer_size);
        if (buffer == NULL) {
            return -1;
        }
        *lineptr = buffer;
        *n = current_buffer_size;
    }

    size_t pos = 0;
    int c;
    while ((c = fgetc(stream)) != EOF) {
        if (pos >= current_buffer_size - 1) { // Need more space
            current_buffer_size += buffer_increment;
            buffer = (char *)realloc(buffer, current_buffer_size);
            if (buffer == NULL) {
                return -1;
            }
            *lineptr = buffer;
            *n = current_buffer_size;
        }
        buffer[pos++] = (char)c;
        if (c == '\n') {
            break;
        }
    }

    if (pos == 0 && c == EOF) {
        return -1; // No characters read and EOF reached
    }

    buffer[pos] = '\0'; // Null-terminate the string
    return (ssize_t)pos;
}

int isFileEmpty(const char *filename) {
    FILE *file = fopen(filename, "rb");
    if (file == NULL) {
        return 1; // File doesn't exist or can't be opened
    }

    fseek(file, 0, SEEK_END);
    long size = ftell(file);
    fclose(file);

    return size == 0;
}

char** readAllLinesFromFile(const char *filename) {
    FILE *file = fopen(filename, "r");
    if (file == NULL) {
        perror("Error opening file");
        return NULL;
    }

    char **lines = NULL;
    size_t capacity = 0;
    size_t size = 0;
    char *buffer = NULL;
    size_t bufsize = 0;
    ssize_t linelen;

    while ((linelen = custom_getline(&buffer, &bufsize, file)) != -1) {
        if (size >= capacity) {
            capacity = capacity > 0 ? capacity * 2 : 1;
            char **new_lines = realloc(lines, capacity * sizeof(char *));
            if (!new_lines) {
                free(buffer);
                for (size_t i = 0; i < size; i++) {
                    free(lines[i]);
                }
                free(lines);
                fclose(file);
                return NULL;
            }
            lines = new_lines;
        }
        lines[size++] = strdup(buffer);
    }

    free(buffer);
    fclose(file);

    if (size == 0) {
        free(lines);
        return NULL;
    }

    lines = realloc(lines, (size + 1) * sizeof(char *));
    if (!lines) {
        // In the unlikely event realloc fails here, the original lines is lost.
        return NULL;
    }
    lines[size] = NULL;

    return lines;
}

int appendLineToFile(const char *filename, const char *line) {
    FILE *file = fopen(filename, "a");
    if (file == NULL) {
        return 1; // Unable to open file
    }

    fprintf(file, "%s\n", line);
    fclose(file);
    return 0; // Success
}
