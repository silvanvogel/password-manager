#include "passwordManager.h"
#include "encryption.h"
#include "fileUtils.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

// Path to password database
const char* ACCOUNTS_FILE = "data/accounts.db";

Account* getAllAccounts(size_t* count) {
    char **lines = readAllLinesFromFile(ACCOUNTS_FILE);
    if (lines == NULL) {
        *count = 0;
        return NULL;
    }

    Account *accounts = NULL;
    size_t accounts_size = 0;
    for (size_t i = 0; lines[i] != NULL; i++) {
        char *line = lines[i];
        char *name = strtok(line, ",");
        char *username = strtok(NULL, ",");
        char *password = strtok(NULL, ",");

        if (name && username && password) {
            accounts = realloc(accounts, (accounts_size + 1) * sizeof(Account));
            if (!accounts) {
                free(accounts);
                *count = 0;
                return NULL;
            }

            strncpy(accounts[accounts_size].name, name, sizeof(accounts[accounts_size].name));
            strncpy(accounts[accounts_size].username, username, sizeof(accounts[accounts_size].username));
            strncpy(accounts[accounts_size].password, password, sizeof(accounts[accounts_size].password));
            accounts_size++;
        }

        free(line);
    }
    free(lines);

    *count = accounts_size;
    return accounts; // Caller is responsible for freeing this memory
}

int addAccount(const Account* account) {
    char *encryptedPassword = encryptPassword(account->password);
    if (encryptedPassword == NULL) {
        printf("Failed to encrypt the password.\n");
        return 1; // Encryption failed
    }

    char line[1024];
    snprintf(line, sizeof(line), "%s,%s,%s", account->name, account->username, encryptedPassword);
    int status = appendLineToFile(ACCOUNTS_FILE, line);

    free(encryptedPassword);
    return status; // 0 for success, non-zero for failure
}

char* getPassword(const Account* account) {
    if (account == NULL || account->password[0] == '\0') return NULL;

    // Decrypt the password
    return decryptPassword(account->password);
}

int updateAccount(size_t index, const Account* newAccount) {
    size_t count;
    Account* accounts = getAllAccounts(&count);

    if (accounts == NULL || index >= count) {
        free(accounts);
        return 1; // Index out of range or no accounts
    }

    // Copy and ensure null termination for name
    strncpy(accounts[index].name, newAccount->name, sizeof(accounts[index].name) - 1);
    accounts[index].name[sizeof(accounts[index].name) - 1] = '\0';

    // Copy and ensure null termination for username
    strncpy(accounts[index].username, newAccount->username, sizeof(accounts[index].username) - 1);
    accounts[index].username[sizeof(accounts[index].username) - 1] = '\0';

    // Check if the password was changed
    if (strcmp(accounts[index].password, newAccount->password) != 0) {
        // If different, encrypt the new password
        char *encryptedPassword = encryptPassword(newAccount->password);
        if (encryptedPassword == NULL) {
            free(accounts);
            return 1; // Encryption failed
        }
        strncpy(accounts[index].password, encryptedPassword, sizeof(accounts[index].password) - 1);
        accounts[index].password[sizeof(accounts[index].password) - 1] = '\0';
        free(encryptedPassword);
    }

    // Rewrite all accounts to the file
    int status = 0;
    remove(ACCOUNTS_FILE); // Remove the old file
    for (size_t i = 0; i < count; i++) {
        char *encryptedPassword = encryptPassword(accounts[i].password);
        if (encryptedPassword == NULL) {
            status = 1; // Encryption failed
            break;
        }

        char line[1024];
        int written = snprintf(line, sizeof(line), "%s,%s,%s", accounts[i].name, accounts[i].username, accounts[i].password);
        if (written < 0 || written >= sizeof(line)) {
            // Handle snprintf error or truncation
            status = 1;
            break;
        }

        if (appendLineToFile(ACCOUNTS_FILE, line) != 0) {
            status = 1; // Writing to file failed
            break;
        }
    }

    free(accounts);
    return status;
}

int deleteAccount(size_t index) {
    size_t count;
    Account* accounts = getAllAccounts(&count);

    if (accounts == NULL || index >= count) {
        free(accounts);
        return 1; // Index out of range or no accounts
    }

    // Shift all accounts after the deleted account one up
    for (size_t i = index; i < count - 1; i++) {
        accounts[i] = accounts[i + 1];
    }

    // Rewrite all accounts to the file, excluding the last one
    int status = 0;
    remove(ACCOUNTS_FILE); // Remove the old file
    for (size_t i = 0; i < count - 1; i++) {
        char line[1024];
        int written = snprintf(line, sizeof(line), "%s,%s,%s", accounts[i].name, accounts[i].username, accounts[i].password);
        if (written < 0 || written >= sizeof(line)) {
            status = 1; // truncation error
            break;
        }

        if (appendLineToFile(ACCOUNTS_FILE, line) != 0) {
            status = 1; // Writing to file failed
            break;
        }
    }

    free(accounts);
    return status;
}

int checkPasswordStrength(const char* password) {
    int score = 0;

    if (strlen(password) >= 8) score += 1; // Length at least 8 characters
    if (strpbrk(password, "0123456789")) score += 1; // Contains digits
    if (strpbrk(password, "ABCDEFGHIJKLMNOPQRSTUVWXYZ")) score += 1; // Contains uppercase letters
    if (strpbrk(password, "abcdefghijklmnopqrstuvwxyz")) score += 1; // Contains lowercase letters
    if (strpbrk(password, "!@#$%^&*()-_=+[]{};:,.<>?")) score += 1; // Contains special characters

    return score;
}

char* generateSecurePassword(int length) {
    if (length < 8) {
        length = 8;  // Ensure minimum length of 8
    }

    static const char upperCase[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    static const char lowerCase[] = "abcdefghijklmnopqrstuvwxyz";
    static const char numbers[] = "0123456789";
    static const char specialChars[] = "!@#$%^&*()_+";

    char* password = malloc(sizeof(char) * (length + 1));
    if (!password) {
        return NULL;
    }

    // Seed the random number generator
    srand((unsigned int)time(NULL));

    // Each category must be represented in the password
    password[0] = upperCase[rand() % strlen(upperCase)];
    password[1] = lowerCase[rand() % strlen(lowerCase)];
    password[2] = numbers[rand() % strlen(numbers)];
    password[3] = specialChars[rand() % strlen(specialChars)];

    // Fill the rest of the password randomly from all character sets
    for (int i = 4; i < length; i++) {
        static const char allChars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+";
        password[i] = allChars[rand() % (sizeof(allChars) - 1)];
    }

    password[length] = '\0';
    return password;
}
