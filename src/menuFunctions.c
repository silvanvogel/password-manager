#include "menuFunctions.h"
#include "masterPasswordManager.h"
#include "fileUtils.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const char* mainMenuOptions[MENU_OPTIONS_COUNT] = {
    "Manage Accounts",
    "Manage Master Password",
    "Check Password Strength",
    "Generate Secure Password",
    "Exit"
};

const char* accountsMenuOptions[ACCOUNTS_MENU_OPTIONS_COUNT] = {
    "List all Accounts",
    "Add Account",
    "Retrieve Password",
    "Edit Account",
    "Delete Account",
    "Back to Main Menu"
};

const char* masterPasswordMenuOptions[MASTER_PASSWORD_MENU_OPTIONS_COUNT] = {
    "Change Master Password",
    "Back to Main Menu"
};

// Function pointer type for menu functions (used in pauseAndExecute(MenuFunction nextFunction)
typedef void (*MenuFunction)(void);

void clearScreen() {
    // Print enough new lines to simulate clearing the screen
    for (int i = 0; i < 100; ++i) printf("\n");
}

void displayHeader() {
    clearScreen();
    printf("+--------------------------------------------------------------+\n");
    printf("|                === KURAGARI - KEY GUARDIAN ===               |\n");
    printf("+--------------------------------------------------------------+\n");
}

void displayMenuTitle(const char* title) {
    const int headerWidth = 62;
    int titleLength = strlen(title);
    int padding = (headerWidth - titleLength - 2) / 2;      // 2 for spaces and dashes around the title

    printf("\n+");
    for (int i = 0; i < padding; i++) printf("-");
    printf(" %s ", title);
    for (int i = 0; i < padding; i++) printf("-");
    if ((headerWidth - titleLength) % 2 != 0) printf("-");  // Add an extra dash if needed for odd lengths
    printf("+\n\n");
}

void pauseAndExecute(MenuFunction nextFunction) {
    printf("\nPress Enter to continue...");
    getchar(); // Wait for user to press Enter
    if (nextFunction != NULL) {
        nextFunction(); // Execute the passed function
    }
}

void clearBuffer() {
    int c;
    while ((c = getchar()) != '\n' && c != EOF) {}
}

void printAccountTableHeader() {
    printf("+------+---------------------------+---------------------------+\n");
    printf("| No.  | Name                      | Username                  |\n");
    printf("+------+---------------------------+---------------------------+\n");
}

void printAccountTableFooter() {
    printf("+------+---------------------------+---------------------------+\n");
}


void printAccountRow(const Account* account, size_t index) {
    char formattedName[NAME_COL_WIDTH + 1];
    char formattedUsername[USERNAME_COL_WIDTH + 1];
    char formattedNo[NO_COL_WIDTH + 1];

    // Format each field with fixed width
    snprintf(formattedNo, sizeof(formattedNo), "%-*zu", NO_COL_WIDTH, index + 1);
    snprintf(formattedName, sizeof(formattedName), "%-*.*s", NAME_COL_WIDTH, NAME_COL_WIDTH, account->name);
    snprintf(formattedUsername, sizeof(formattedUsername), "%-*.*s", USERNAME_COL_WIDTH, USERNAME_COL_WIDTH, account->username);

    printf("| %s | %s | %s |\n", formattedNo, formattedName, formattedUsername);
}

int verifyMasterPasswordOnStartup() {
    char masterPassword[MASTER_PASSWORD_MAX_LEN];

    if (isFileEmpty(MASTER_HASH_FILE)) {
        // Set new master password
        printf("\nNo master password set. Please set a new master password: ");


        if (fgets(masterPassword, sizeof(masterPassword), stdin) == NULL) {
            printf("Error reading master password.\n");
            return 1;
        }

        // Remove newline character if present
        size_t len = strlen(masterPassword);
        if (len > 0 && masterPassword[len - 1] == '\n') {
            masterPassword[len - 1] = '\0';
        }

        if (setMasterPassword(masterPassword) == 0) {
            printf("Master password set successfully.\n");
            return 0;
        } else {
            printf("Failed to set master password.\n");
            return 1;
        }
    } else {
        // Verify master password
        printf("\nEnter master password to unlock: ");

        if (fgets(masterPassword, sizeof(masterPassword), stdin) == NULL) {
            printf("Error reading master password.\n");
            return 1;
        }

        // Remove newline character if present
        size_t len = strlen(masterPassword);
        if (len > 0 && masterPassword[len - 1] == '\n') {
            masterPassword[len - 1] = '\0';
        }

        if (verifyMasterPassword(masterPassword) == 0) {
            printf("Master password verified successfully.\n");
            return 0;
        } else {
            printf("Master password verification failed.\n");
            return 1;
        }
    }
}

void changeMasterPasswordProcedure() {
    char currentPassword[MASTER_PASSWORD_MAX_LEN];
    char newPassword[MASTER_PASSWORD_MAX_LEN];
    char confirmPassword[MASTER_PASSWORD_MAX_LEN];

    displayHeader();
    displayMenuTitle("Change Master Password");

    // Prompt for the current master password
    printf("Enter current master password: ");
    fgets(currentPassword, sizeof(currentPassword), stdin);
    currentPassword[strcspn(currentPassword, "\n")] = 0; // Remove newline character

    // Verify the current master password
    if (verifyMasterPassword(currentPassword) == 0) {
        // Prompt for new master password
        printf("Enter new master password: ");
        fgets(newPassword, sizeof(newPassword), stdin);
        newPassword[strcspn(newPassword, "\n")] = 0; // Remove newline character

        int strength = checkPasswordStrength(newPassword);
        if (strength < 3) {
            printf("Password is weak. Try a stronger password.\n");
            pauseAndExecute(changeMasterPasswordProcedure);
            return;
        }

        // Confirm new master password
        printf("Confirm new master password: ");
        fgets(confirmPassword, sizeof(confirmPassword), stdin);
        confirmPassword[strcspn(confirmPassword, "\n")] = 0; // Remove newline character

        // Check if new password and confirmation match
        if (strcmp(newPassword, confirmPassword) == 0) {
            // Change to the new master password
            if (changeMasterPassword(currentPassword, newPassword) == 0) {
                printf("Master password changed successfully.\n");
            } else {
                printf("Failed to change master password.\n");
            }
        } else {
            printf("\nPasswords do not match.\n");
        }
    } else {
        printf("\nIncorrect master password.\n");
    }

    pauseAndExecute(manageMasterPasswordMenu);
}

void checkPasswordStrengthMenu() {
    displayHeader();
    displayMenuTitle("Password Strength Checker");

    char testPassword[MASTER_PASSWORD_MAX_LEN];

    printf("Enter a password to check its strength: ");
    fgets(testPassword, sizeof(testPassword), stdin);
    testPassword[strcspn(testPassword, "\n")] = 0; // Remove newline character

    int strength = checkPasswordStrength(testPassword);
    printf("Password strength score: %d (out of 5)\n", strength);

    pauseAndExecute(displayMainMenu);
}

void generatePasswordMenu() {
    displayHeader();
    displayMenuTitle("Generate Secure Password");

    int length;
    printf("Enter desired password length (min. 8 characters): ");
    scanf("%d", &length);
    getchar(); // Consume trailing newline char

    // Validate password length
    // TODO: use MAX_PASSWORD_LENGTH -> if (length <= 0 || length > MAX_PASSWORD_LENGTH) {
    if (length < 8 || length > 100) {
        printf("Invalid length. Password length must be between 8 and 100 characters.\n");
        pauseAndExecute(generatePasswordMenu);
        return;
    }

    char* password = generateSecurePassword(length);
    if (password) {
        printf("Generated Password: %s\n", password);
        free(password); // Free the memory allocated for the password
    } else {
        printf("Error generating password.\n");
    }

    pauseAndExecute(displayMainMenu);
}

void listAllAccountsMenu() {
    displayHeader();
    displayMenuTitle("All Accounts");

    size_t count;
    Account* accounts = getAllAccounts(&count);

    if (accounts == NULL || count == 0) {
        printf("No accounts found.\n");
    } else {
        printAccountTableHeader();

        for (size_t i = 0; i < count; i++) {
            printAccountRow(&accounts[i], i);
            printAccountTableFooter();
        }
    }

    // Free the memory allocated by getAllAccounts
    free(accounts);

    pauseAndExecute(manageAccountsMenu);
}

void addAccountMenu() {
    displayHeader();
    displayMenuTitle("Add New Account");

    Account newAccount;

    printf("Enter Account Name: ");
    fgets(newAccount.name, sizeof(newAccount.name), stdin);
    newAccount.name[strcspn(newAccount.name, "\n")] = 0; // Remove newline character

    printf("Enter Username: ");
    fgets(newAccount.username, sizeof(newAccount.username), stdin);
    newAccount.username[strcspn(newAccount.username, "\n")] = 0; // Remove newline character

    printf("Enter Password: ");
    fgets(newAccount.password, sizeof(newAccount.password), stdin);
    newAccount.password[strcspn(newAccount.password, "\n")] = 0; // Remove newline character

    if (addAccount(&newAccount) == 0) {
        printf("Account added successfully.\n");
    } else {
        printf("Failed to add account.\n");
    }

    pauseAndExecute(manageAccountsMenu);
}

void getPasswordMenu() {
    displayHeader();
    displayMenuTitle("Retrieve Account Password");

    size_t count;
    Account* accounts = getAllAccounts(&count);
    if (accounts == NULL || count == 0) {
        printf("No accounts available.\n");
        free(accounts);
        pauseAndExecute(manageAccountsMenu);
        return;
    }

    printf("Select the account to retrieve the password for:\n");
    for (size_t i = 0; i < count; i++) {
        printf("%zu: %s\n", i + 1, accounts[i].name);
    }
    printf("\nEnter the number of the account (or 0 to cancel): ");
    size_t choice;
    scanf("%zu", &choice);
    getchar(); // consume newline

    if (choice == 0 || choice > count) {
        printf("Operation cancelled or invalid choice.\n");
    } else {
        size_t index = choice - 1; // Adjust for 0-based index
        char *password = getPassword(&accounts[index]);
        if (password) {
            printf("Password for '%s': %s\n", accounts[index].name, password);
            free(password); // Free the memory allocated by getPassword
        } else {
            printf("Failed to retrieve password.\n");
        }
    }

    free(accounts);
    pauseAndExecute(manageAccountsMenu);
}

void updateAccountMenu() {
    displayHeader();
    displayMenuTitle("Edit Account");

    size_t count;
    Account* accounts = getAllAccounts(&count);
    if (accounts == NULL || count == 0) {
        printf("No accounts available to update.\n");
        free(accounts);
        pauseAndExecute(manageAccountsMenu);
        return;
    }

    printAccountTableHeader();
    for (size_t i = 0; i < count; i++) {
        printAccountRow(&accounts[i], i);
    }
    printAccountTableFooter();

    printf("\nEnter the number of the account (or 0 to cancel): ");
    size_t choice;
    scanf("%zu", &choice);
    getchar(); // consume newline

    if (choice == 0 || choice > count) {
        printf("Operation cancelled or invalid choice.\n");
        free(accounts);
        pauseAndExecute(manageAccountsMenu);
        return;
    }

    // Assuming choice is 1-based index
    size_t index = choice - 1;
    Account updatedAccount;

    printf("Enter new name (current: %s): ", accounts[index].name);
    fgets(updatedAccount.name, sizeof(updatedAccount.name), stdin);
    updatedAccount.name[strcspn(updatedAccount.name, "\n")] = 0; // Remove newline
    if (strlen(updatedAccount.name) == 0) {
        strncpy(updatedAccount.name, accounts[index].name, sizeof(updatedAccount.name));
    }

    printf("Enter new username (current: %s): ", accounts[index].username);
    fgets(updatedAccount.username, sizeof(updatedAccount.username), stdin);
    updatedAccount.username[strcspn(updatedAccount.username, "\n")] = 0;
    if (strlen(updatedAccount.username) == 0) {
        strncpy(updatedAccount.username, accounts[index].username, sizeof(updatedAccount.username));
    }

    printf("Enter new password: ");
    fgets(updatedAccount.password, sizeof(updatedAccount.password), stdin);
    updatedAccount.password[strcspn(updatedAccount.password, "\n")] = 0;
    if (strlen(updatedAccount.password) == 0) {
        strncpy(updatedAccount.password, accounts[index].password, sizeof(updatedAccount.password));
    }

    if (updateAccount(index, &updatedAccount) == 0) {
        printf("Account successfully updated.\n");
    } else {
        printf("Failed to update account.\n");
    }

    free(accounts);
    pauseAndExecute(manageAccountsMenu);
}

void deleteAccountMenu() {
    displayHeader();
    displayMenuTitle("Delete Account");

    size_t count;
    Account* accounts = getAllAccounts(&count);
    if (accounts == NULL || count == 0) {
        printf("No accounts available to delete.\n");
        free(accounts);
        pauseAndExecute(manageAccountsMenu);
        return;
    }

    printAccountTableHeader();
    for (size_t i = 0; i < count; i++) {
        printAccountRow(&accounts[i], i);
    }
    printAccountTableFooter();

    printf("\nEnter the number of the account to delete (or 0 to cancel): ");
    size_t choice;
    scanf("%zu", &choice);
    getchar(); // consume newline

    if (choice == 0 || choice > count) {
        printf("Operation cancelled or invalid choice.\n");
    } else {
        size_t index = choice - 1; // Adjust for 0-based index
        if (deleteAccount(index) == 0) {
            printf("Account successfully deleted.\n");
        } else {
            printf("Failed to delete account.\n");
        }
    }

    free(accounts);
    pauseAndExecute(manageAccountsMenu);
}

void manageMasterPasswordMenu() {
    displayHeader();
    displayMenuTitle("Manage Master Password");

    int choice;
    
    for (int i = 0; i < MASTER_PASSWORD_MENU_OPTIONS_COUNT; i++) {
        printf("%d. %s\n", i + 1, masterPasswordMenuOptions[i]);
    }
    printf("\nChoose an option (1-%d): ", MASTER_PASSWORD_MENU_OPTIONS_COUNT);

    if (scanf("%d", &choice) != 1 || choice < 1 || choice > MASTER_PASSWORD_MENU_OPTIONS_COUNT) {
        printf("Invalid choice. Please try again.\n");
        clearBuffer(); // Clear input buffer
        pauseAndExecute(manageMasterPasswordMenu);
        return;
    }

    getchar(); // consume newline char

    switch (choice) {
        case 1:
            changeMasterPasswordProcedure();
            break;
        case 2:
            displayMainMenu();
            break;
        default:
            printf("Invalid choice. Please try again.\n");
    }
}

void manageAccountsMenu() {
    displayHeader();
    displayMenuTitle("Manage Accounts");

    int choice;
    
    for (int i = 0; i < ACCOUNTS_MENU_OPTIONS_COUNT; i++) {
        printf("%d. %s\n", i + 1, accountsMenuOptions[i]);
    }
    printf("\nChoose an option (1-%d): ", ACCOUNTS_MENU_OPTIONS_COUNT);

    if (scanf("%d", &choice) != 1 || choice < 1 || choice > ACCOUNTS_MENU_OPTIONS_COUNT) {
        printf("Invalid choice. Please try again.\n");
        clearBuffer(); // Clear input buffer
        pauseAndExecute(manageAccountsMenu);
        return;
    }

    getchar(); // consume newline char

    switch (choice) {
	    case 1:
 	        listAllAccountsMenu();
		    break;
	    case 2:
            addAccountMenu();
            break;
        case 3:
            getPasswordMenu();
            break;
        case 4:
            updateAccountMenu();
            break;
        case 5:
            deleteAccountMenu();
            break;
        case 6:
            displayMainMenu();
            break;
        default:
            printf("Invalid choice. Please try again.\n");
    }
}

void displayMainMenu() {
    displayHeader();
    displayMenuTitle("Main Menu");

    int choice;

    for (int i = 0; i < MENU_OPTIONS_COUNT; i++) {
        printf("%d. %s\n", i + 1, mainMenuOptions[i]);
    }
    printf("\nChoose an option (1-%d): ", MENU_OPTIONS_COUNT);

    if (scanf("%d", &choice) != 1 || choice < 1 || choice > MENU_OPTIONS_COUNT) {
        printf("Invalid choice. Please try again.\n");
        clearBuffer(); // Function to clear input buffer
        pauseAndExecute(displayMainMenu);
        return;
    }

    getchar(); // consume newline char

    switch (choice) {
        case 1:
            manageAccountsMenu();
            break;
        case 2:
            manageMasterPasswordMenu();
            break;
        case 3:
            checkPasswordStrengthMenu(); 
            break;
        case 4:
            generatePasswordMenu();
            break;
        case 5:
            printf("Exiting...\n");
            break;
    }
}
