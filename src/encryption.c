#include "encryption.h"
#include <openssl/rand.h>
#include <openssl/evp.h>
#include <stdlib.h>
#include <string.h>

int hashData(const unsigned char* data, size_t dataLen, unsigned char* salt, unsigned char* hash, int generateSalt) {
    if (generateSalt) {
        // Generate a random salt if requested (1=yes, 0=no)
        if (RAND_bytes(salt, SALT_SIZE) != 1) {
            return 1; // Error in random number generation
        }
    }

    EVP_MD_CTX* mdctx = EVP_MD_CTX_new();
    if (mdctx == NULL) {
        return 1; // Allocation failure
    }

    if (EVP_DigestInit_ex(mdctx, EVP_sha256(), NULL) <= 0) {
        EVP_MD_CTX_free(mdctx);
        return 1; // Digest initialization failed
    }

    if (EVP_DigestUpdate(mdctx, salt, SALT_SIZE) <= 0) {
        EVP_MD_CTX_free(mdctx);
        return 1; // Digest update failed
    }

    if (EVP_DigestUpdate(mdctx, data, dataLen) <= 0) {
        EVP_MD_CTX_free(mdctx);
        return 1; // Digest update failed
    }

    unsigned int lengthOfHash = 0;
    if (EVP_DigestFinal_ex(mdctx, hash, &lengthOfHash) <= 0) {
        EVP_MD_CTX_free(mdctx);
        return 1; // Digest finalization failed
    }

    EVP_MD_CTX_free(mdctx);
    return 0; // Success
}

char* encryptPassword(const char* plaintext) {
    if (plaintext == NULL) return NULL;

    EVP_CIPHER_CTX *ctx;
    int len, ciphertext_len;

    // Create and initialize context
    if (!(ctx = EVP_CIPHER_CTX_new())) return NULL;

    // Initialize encryption
    if (EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv) <= 0) {
        EVP_CIPHER_CTX_free(ctx);
        return NULL;
    }

    // Allocate memory for ciphertext
    unsigned char *ciphertext = malloc(strlen(plaintext) + EVP_MAX_BLOCK_LENGTH);
    if (!ciphertext) {
        EVP_CIPHER_CTX_free(ctx);
        return NULL;
    }

    // Encrypt the plaintext
    if (EVP_EncryptUpdate(ctx, ciphertext, &len, (unsigned char*)plaintext, strlen(plaintext)) <= 0) {
        free(ciphertext);
        EVP_CIPHER_CTX_free(ctx);
        return NULL;
    }
    ciphertext_len = len;

    // Finalize encryption
    if (EVP_EncryptFinal_ex(ctx, ciphertext + len, &len) <= 0) {
        free(ciphertext);
        EVP_CIPHER_CTX_free(ctx);
        return NULL;
    }
    ciphertext_len += len;

    // Convert encrypted data to hexadecimal string
    char *hex_ciphertext = malloc(ciphertext_len * 2 + 1);
    if (!hex_ciphertext) {
        free(ciphertext);
        return NULL;
    }

    for (int i = 0; i < ciphertext_len; i++) {
        sprintf(hex_ciphertext + (i * 2), "%02x", ciphertext[i]);
    }
    hex_ciphertext[ciphertext_len * 2] = '\0';

    // Cleanup
    free(ciphertext);
    EVP_CIPHER_CTX_free(ctx);

    return hex_ciphertext;
}

char* decryptPassword(const char* hex_ciphertext) {
    if (hex_ciphertext == NULL) return NULL;

    int hex_length = strlen(hex_ciphertext);
    int ciphertext_len = hex_length / 2;
    
    unsigned char *ciphertext = malloc(ciphertext_len);
    if (!ciphertext) return NULL;

    for (int i = 0; i < ciphertext_len; i++) {
        sscanf(hex_ciphertext + (i * 2), "%02hhx", &ciphertext[i]);
    }

    EVP_CIPHER_CTX *ctx;
    int len, plaintext_len;

    // Create and initialize context
    if (!(ctx = EVP_CIPHER_CTX_new())) {
        free(ciphertext);
        return NULL;
    }

    // Initialize decryption
    if (EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv) <= 0) {
        EVP_CIPHER_CTX_free(ctx);
        free(ciphertext);
        return NULL;
    }

    // Allocate memory for plaintext
    unsigned char *plaintext = malloc(ciphertext_len + 1); // +1 for null terminator
    if (!plaintext) {
        EVP_CIPHER_CTX_free(ctx);
        free(ciphertext);
        return NULL;
    }

    // Decrypt the ciphertext
    if (EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len) <= 0) {
        free(plaintext);
        EVP_CIPHER_CTX_free(ctx);
        free(ciphertext);
        return NULL;
    }
    plaintext_len = len;

    // Finalize decryption
    if (EVP_DecryptFinal_ex(ctx, plaintext + len, &len) <= 0) {
        free(plaintext);
        EVP_CIPHER_CTX_free(ctx);
        free(ciphertext);
        return NULL;
    }
    plaintext_len += len;
    plaintext[plaintext_len] = 0; // Null-terminate the plaintext

    // Cleanup
    EVP_CIPHER_CTX_free(ctx);
    free(ciphertext);

    return (char*)plaintext;
}

