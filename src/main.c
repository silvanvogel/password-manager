#include "menuFunctions.h"
#include "fileUtils.h"
#include "masterPasswordManager.h"
#include "passwordManager.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
    displayHeader();

    if(verifyMasterPasswordOnStartup() == 0){
	    displayMainMenu();
    }

    return 0;
}

