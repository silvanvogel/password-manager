CC=gcc
CFLAGS=-I include -Wall -Werror -g

LDFLAGS=-L$(STATIC_LIB_PATH) -l:libssl.a -l:libcrypto.a -ldl -lpthread

BUILD_DIR=build
BIN_DIR=$(BUILD_DIR)/bin/linux
OBJ_DIR=$(BUILD_DIR)/obj/linux
SRC_DIR=src

# Source files
SOURCES=$(wildcard $(SRC_DIR)/*.c)

# Object files
OBJECTS=$(SOURCES:$(SRC_DIR)/%.c=$(OBJ_DIR)/%.o)

# Target
TARGET=$(BIN_DIR)/password-manager

# Default target
all: $(TARGET)

$(TARGET): $(OBJECTS)
	@mkdir -p $(@D)
	$(CC) $(OBJECTS) -o $@ $(LDFLAGS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	@mkdir -p $(@D)
	$(CC) $(CFLAGS) -c $< -o $@

# Clean
clean:
	rm -rf $(BUILD_DIR)/*

# Phony targets
.PHONY: all clean

